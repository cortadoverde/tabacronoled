var events = require('events');

function Client(){
    events.EventEmitter.call(this);

    this.connected = false;

    this.set = function( socket ) {
        this.id = socket.id;
        this.socket = socket;
        this.connected = true;
        this.emit('ready', this);
    }

    this.get = function() {
        if( this.socket ) {
            return this.socket
        }
    }

    this.disconnect = function() {
        this.connected = false;
        this.emit('disconnect');
    }

}

Client.prototype.__proto__ = events.EventEmitter.prototype;

module.exports = new Client
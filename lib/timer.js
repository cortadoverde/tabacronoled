var events = require('events');

function Client(){
    events.EventEmitter.call(this);

    this.second = 0;

	this.interval = undefined;

	var self = this;

    this.start = function() {
        console.log('Iniciando Timer');
		this.interval = setInterval(this.onTick, 1000);
		this.emit('start');
    },

    this.stop = function() {
       console.log('Parando Timer');
		if( this.interval ) {
			clearInterval(this.interval);
			this.emit('stop');
		}
    },

    this.onTick = function() {
        var pad = function(i){
			var s = "" + i;
			return s.length == 1 ? "0" + s : s;
		}

		var s = pad(self.second%60);
		var m = pad(parseInt(self.second/60));  
		var h = parseInt(self.second/(60*60));  

		self.second++;

		self.emit('tick',""+h + m + s );
    }



}

Client.prototype.__proto__ = events.EventEmitter.prototype;

module.exports = Client;
var io           = require('socket.io')(2324),
    os           = require('os'),
    ManagerEvent = require('./io.listener'),
    Tablero      = require('./tcp.listener'),
    isTcpReady   = false,
    interfaces = os.networkInterfaces();
    address;



  for (var k in interfaces) {
      for (var k2 in interfaces[k]) {
          var address = interfaces[k][k2];
          if (address.family === 'IPv4' && !address.internal) {
              address = address.address;
              start();
          }
      }
  }

  function start() {
    var ip = address.substring(0,address.lastIndexOf(".")) + '.30';
    console.log(ip);
    console.log('iniciando Tablero')
    // Tratar de establecer conexion con el tablero
    Tablero.connect(8899, ip);
  }



  Tablero.on('ready',function(){
      isTcpReady = true;
  });

  Tablero.on('disconnect', function(){
      isTcpReady = false;
  })

  Tablero.on('error', function(){
      console.log('Imposible establecer una conexion con el Tablero');
      isTcpReady = false;
  })


io.sockets.on('connection', function(client){
    ManagerEvent.set(client);

    client.on('disconnect', function(){
        ManagerEvent.disconnect();
    })
});


ManagerEvent.on('ready',function(a){
  console.log('[Manager Event] Ready');

  var Me = this.socket;
  Me.emit('tablero error command')
  Me.on('command', function(command){
    if( isTcpReady ) {
        console.log(command)
        Tablero.socket.write(command, function( Response ){
          Me.emit('tablero out', Response);
        });
    } else {
        console.log(' No hay conexion con el tablero -> ' + command);
        Me.emit('tablero error command')
    }
  })


  Tablero.on('ready',function(){
      Me.emit('tcp ready');
  });

  Tablero.on('disconnect', function(){
      Me.emit('tcp off', 'disconnect');
  })

  Tablero.on('error', function(){
      Me.emit('tcp off', 'error');
  })
})



ManagerEvent.on('disconnect', function(){
    console.info('Atencion no hay conexion con el manejador de eventos');
})

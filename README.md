#Instalacion
1. Posicionarse en el directorio de instalación del repositorio => ```%PATH%/```
2. Si se descarga por primera se debe hacer una clonación del repo ```git clone https://psamudia@bitbucket.org/tabasoftware/tabacronoled.git```
3. En de que ya tenga clonado el repositorio, se deberá actualizar con el siguiente comando ```git pull```
4. Dentro de la carpete del repositorio debera ejecutar el siguiente comando ```node app``` para iniciar la interfaz grafrica que le habilitara el acceso a localhost:2323 -> posteriormente se deberá poder indicar por parametro adicional el puerto.

# Referencias

- 0 - ```[0-9]```           Brillo
- 1 - ```[0-2]```           Modo ( 0 = StdIn, 1 = Temperatura + StdIn, 2 = H + StdIn, 3 = Switch)
- 2 - ```[A-Z0-9\s]```      Slot Izquierdo 0 (Si0) Alfanumerico
- 3 - ```[A-Z0-9\s]```      Slot Izquierdo 1 (Si1) Alfanumerico
- 4 - ```[A-Z0-9\s]```      Slot Izquierdo 2 (Si2) Alfanumerico
- 5 - ```U|D|\s```          Flechas U = Up D = Down \s = espacio ( desactivado )
- 6 - ```[0-2]```           Puntos divisores (:) -> 0 = Sin puntos, 1 = [Sd2,Sd3] 2 = [Sd0,Sd1][Sd2,Sd3]
- 7 - ```[A-Z0-9\*\s]```    Slot Derecho 0 (Sd0) Alfanumerico + caracter semaforo (*)
- 8 - ```[A-Z0-9\*\s]```    Slot Derecho 1 (Sd1) Alfanumerico + caracter semaforo (*)
- 9 - ```[A-Z0-9\*\s]```    Slot Derecho 2 (Sd2) Alfanumerico + caracter semaforo (*)
- 10 - ```[A-Z0-9\*\s]```   Slot Derecho 3 (Sd3) Alfanumerico + caracter semaforo (*)
- 11 - ```[A-Z0-9\*\s]```   Slot Derecho 4 (Sd4) Alfanumerico + caracter semaforo (*)
- 12 - ```\r```             Retorno de carro para finalizar comando


#Ciclo de proceso

la ```App``` dispone una interface gráfica para generar el ```Comando``` que es gestionado
por el ```Manager Event (ME)``` y se lo deriva a al ```Manager Tablero``` el cual se conecta
via socket al Tablero LED y le emite la cadena según la referencia técnica del Tablero y devuelve una ```Respuesta``` variable.


    +-----+       +------+       +------+       +---+---+---+---+---+---+---+---+---+
    | APP | <---> |  ME  | <---> |  MT  | <---> | 8 | 8 | 8 | ^ | 8 | 8 | 8 | 8 | 8 |
    +-----+       +------+       +------+       +---+---+---+---+---+---+---+---+---+



## Composición cadena

    +-------+---+---------------+
    | A B C | v |     0 1 : 0 0 | => 10ABCD1 0100
    +-------+---+---------------+
        1 -> Luminocidad
        0 -> Modo entrada Slots izquierdos de forma manual ( StdIn )
        A -> Si0
        B -> Si1
        C -> Si2
        D -> Flecha Down ( v )
        1 -> Indicador de separador de tiempo (m:s)
          -> Sd0 ( slot vacio hora )
        0 -> Sd1
        1 -> Sd2
        0 -> Sd3
        0 -> Sd4

    +-------+---+---------------+
    | A B C | ^ | 1 : 0 1 : 0 0 | => 10ABCU210100
    +-------+---+---------------+
        1 -> Luminocidad
        0 -> Modo entrada Slots izquierdos de forma manual ( StdIn )
        A -> Si0
        B -> Si1
        C -> Si2
        U -> Flecha Up ( ^ )
        2 -> Indicador de separador de tiempo (h:m:s)
        1 -> Sd0  
        0 -> Sd1
        1 -> Sd2
        0 -> Sd3
        0 -> Sd4

## Respuesta
  Si el modo esta seteado en 3  

## Modos de procesos

El modo de proceso genera una rutina o proceso que puede interactuar con diferentes variables para
emitir una comando al ```ME```, básicamente existe un modo comando I/O, que es utilizada para avisarle
al tablero que debe mostrar algo, el tablero solo maneja la lógica de parseo para el comando o instrucción.

Por eso se deben distinguir diferentes modos para los casos que iremos exponiendo. **Esta documentación
inicial pretende plantear los campos de prueba para poder organizar.**


### Modo comando

*@todo: documentar*


### Modo cuenta regresiva

*@todo: documentar*


### Modo cronometro

*@todo: documentar*


## Modo Partido

Define el inicio del cronometro segun la salida de los participantes, **quedaría pendiente
definir si este tipo de modo solo se aplica con partidas en intervalos y por paticipantes,
o si puede variar segun el tipo de competicion**

### Partidor por intervalo

- Este submodo permite cargar una lista de participantes con el siguiente formato

    'Nro de Moto'\n\r

- Se definira el intervalo de tiempo en el que deberan ir saliendo los competidores, hay que
tener en cuenta el tiempo de demora de la cuenta regresiva para que le fin del intervalo del
semaforo coincida con la diferencia de tiempo asignada

- Este modo genera 2 intervalos, el que se recorre cada x cantidad de segundos para que cada
participante salga, y el intervalo de cuenta regresiva del participante activo, asi mismo al
finalizar el primer intervalo de cuenta regresiva se debera disparar el cronometro de la
competicion.

Por ejemplo si tenemos 4 participantes con un intervalo de 15 segundos para la salida, se
debera cumplir los siguientes pasos

    Tiempo real en ejecucion
    ------------------------
    [0:00:00 -> 0:00:10]  -> Inicia el intervalo de cuenta regresiva participante 1 ( 10 seg )
    [0:00:10 -> 0:00:00]  -> Inicia el cronometro 0:00:00
    [0:00:15 -> 0:00:25]  -> Inicia el intervalo de cuenta regresiva participante 2 ( 10 seg )
    [0:00:30 -> 0:00:25]  ->

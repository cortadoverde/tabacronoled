"use strict";

/**
 * Dependencias
 */
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 2323;

var clientio = require('socket.io-client');
var tablero  = clientio.connect('http://localhost:2324');

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

app.use(express.static(__dirname + '/public'));

io.sockets.on('connection', function(socket) {
    socket.on('command', function(data){
        console.log('app -> tablero -> command -> ' + data);
        tablero.emit('command', data);
    })
})
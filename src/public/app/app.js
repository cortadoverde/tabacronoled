var TabaApp = TabaApp || angular.module('TabaApp', ['socket.io'] );

TabaApp.config(function($socketProvider){
    $socketProvider.setConnectionUrl('http//localhost:2323');
})

TabaApp.controller('Ctrl', function Ctrl($scope, $socket){

    $scope.send = function() {
        $socket.emit('command', $scope.tablero.command );
    }
})
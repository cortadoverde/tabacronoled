  var socket = io(); // TIP: io() with no args does auto-discovery
  
  var timerActive;

  function startTimer(duration, callback) { 
    var timer = parseInt(duration), h, m, s;
     
    if( typeof timerActive == "undefined" ) {
      timerActive = setInterval(function(){
        h = Math.floor( timer / 3600 );
        m = Math.floor( timer % 3600 / 60 );
        s = timer % 60;
        m = m < 10 ? "0" + m : m;
        s = s < 10 ? "0" + s : s;
        callback([ h.toString() , m.toString() , s.toString()]);
        if( --timer < 0 )
          clearTimer();
      },1000);  
    }
  }

  function clearTimer() {
    if( typeof timerActive == "number" ) {
      clearInterval( timerActive );
      timerActive = undefined;
    }
  }

  function send( data ) {
    data = data.toString().substr(0,12) + '\r';
    socket.emit('command', data);
  }

  function down() {
      var txt = '10';
      var letras = prompt('Slot izquierdo (3 caracteres)');
      var tiempo = prompt('tiempo');

      if( letras.length > 3 ) letras.substr(0,3);
      // Asegurar que no ocupe mas de 3 letras
      if( letras.letras < 3 ) letras = letras + ' '.repeat(3 - letras.length);

      txt = txt + letras + 'D0';

      startTimer(tiempo, function(arr){
        displayTimeTablero(arr, txt);
      })


  } 

  function semaforo() {
     var txt = '10ESP 0',
        current_slot = '',
        repeat = 0,
        maxRepeat = 10,
        interval;

     var intermitent = function() {
        interval = setInterval(function(){
          var intencity = txt.substr(0,1);
          if( intencity == 9 ) {
            txt = "1" + txt.substr(1) 
          } else {
            txt = "9" + txt.substr(1) 
          }
          
          repeat++;
          if( repeat > maxRepeat ) {
            console.log('Adelante 10\sGO\s0\s\s\s\s\s')
            send('10 GO 0     ');
            clearInterval(interval);
          }else{
            send( txt );
          }
       },10)
     }

     var init = function () {           
         setTimeout(function () {    
            current_slot = current_slot + '*';
            if (current_slot.length <= 5 ) {
               var ntxt  = txt + current_slot + ' '.repeat(5 - current_slot.length);
               send( ntxt );             
               init();             
            } else {
               txt = txt + current_slot;
               intermitent();
            }                       
         }, 1000)
      }

      init();  
     
  }



function displayTimeTablero(arr, display) {
  var txt = [];
  // arr.length -> 2 m:s 
  // arr.length -> 3 h:m:s
  var h = 0, m = 00, s = 00;
  
  if( arr.length == 3 ) {
    var h = arr[0], m = arr[1], s = arr[2];
  }else if( arr.length == 2 ) {
    var m = arr[0], s = arr[1];
  }

  txt.push(h.toString().substr(0,1));
  txt.push(m.toString().substr(0,1));
  txt.push(m.toString().substr(1,1));
  txt.push(s.toString().substr(0,1));
  txt.push(s.toString().substr(1,1));

  send(display + txt.join(''));

}

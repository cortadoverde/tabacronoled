var Client = function( name, config) {
    this.name = name;
    this.config = config;  
    this.active = false;

    this.connect = function(socket, callback ) {
      this.socket = socket;
      this.active = true;
      this.response(200);
      if( typeof callback == 'function' ) {
        callback.apply(this, [this]);
      }

    }

    this.disconnect = function( callback ) {
        var c = callback || function(){};
        this.active = false;
        this.response(404)
        if( typeof callback == 'function' ) {
          callback.apply(this, [this]);
        }
    }

    this.emit = function() {

      if( this.isConnected() ) {
        var args = Array.prototype.slice.call(arguments);
        return this.socket.emit.apply(this.socket, args);
      } else {

        this.response(300)
      }      
    }

    this.isConnected = function() {
      return this.socket && this.socket.socket && this.socket.socket.connected;
    }

    this.on = function() {
      if( this.isConnected() ) {
        var args = Array.prototype.slice.call(arguments);
        return this.socket.on.apply(this.socket, args);
      } else {
        this.response(300)
      }
    }

    this.response = function( code )
    {
      var e_codes = {
        200 : 'Conectado',
        300 : 'Socket inaccesible',
        404 : 'Desconectado',
        999 : 'Error desconocido'
      };

      var strCode = typeof e_codes[code] == 'undefined' ? e_codes[999] : e_codes[code];

      
      console.log(['[', code, '][', this.name, '] - ' , strCode].join(''));
    }
}

module.exports = Client;
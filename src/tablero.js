var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  , tcpsock = require('net')
  , tabaClient = require('./client')
  , dataConn = {
        host: "10.10.100.254",
        port: 8899
    };
 
server.listen(2324);
 
app.get('/', function (req, res) {
    return res.send('');
});

 
var tcpClient = new tcpsock.Socket(),
    
    managerEvent = new tabaClient('Manager Event'),

    tablero = {
      socket : null,
      active : false,
      connect: function( socket ) {
        this.active = true;
        this.socket = socket;
        console.log('Conexion TCP lista')
      },
      disconnect: function() {
        this.active = false;
        console.log('Desconexion TCP');
      },
      emit : function( data ) {
        this.socket.write(data);
      },

      start : function() {
        self = this;

        tcpClient.setKeepAlive(true);

        tcpClient.connect(dataConn.port, dataConn.host, function(){
            self.connect(tcpClient);    
        });

      }
    };

io.sockets.on('connection', function (socket) {   
    managerEvent.connect(socket, function(m){
      console.log(m.name)
    });
    socket.on('command', function(data) {
        if( tablero.active )
          tablero.emit(data);        
    });

    socket.on('connect tablero', function(){
        tablero.start();
    })
});


tablero.start();

tcpClient.on('close',function(){
    console.log('disconnect');
})

tcpClient.on('error', function(e){
  console.log( 'error');  
})


managerEvent.on('ping', function(){
  console.log('pong');
})


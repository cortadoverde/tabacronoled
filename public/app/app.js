window.onbeforeunload = function(e){
  return 'Seguro que desea cerrar, es posible que se pierda conexion con el gestor de eventos';
}

var TabaApp = TabaApp ||
    angular.module(
      'TabaApp', [
        'socket.io',
        'ui-rangeSlider',
        'ui.bootstrap']
    );

TabaApp
    .config(function($socketProvider){
        $socketProvider.setConnectionUrl('http://SAMIDIAP:2323');
    })
    .run(function($rootScope, $socket){

        $rootScope.lastCommand = false;

        $rootScope.stageActive = 'Comando';
        $rootScope.$on('change tab', function(e,currentTab){
          console.log('change tab', currentTab);
          $rootScope.stageActive = currentTab.title;
        })
        $rootScope.master = {
          intencidad: 1
        }

        $rootScope.tablero = {
          intencidad : 1,
          modo : 0,
          si: 'ABC',
          ud: 'U',
          puntos: 0,
          sd: ''
        }

        $socket.on('connect',function(socket){
        })

        $socket.on('noty', function(d){
        })

        $rootScope.$on('tablero intencidad', function(scope, data){
          $rootScope.master.intencidad = data;
          if( $rootScope.stageActive == 'Comando')
            $rootScope.display();
        })

        $rootScope.$on('tablero debug', function(){
        })

        $rootScope.$on('command', function(e, data){

          $rootScope.outPutLed = data;
          console.log('command emit RootScope ' + data)

          $socket.emit('command', data );
        })

        $rootScope.display = function() {
          var tablero = $rootScope.tablero;
          // Define la intencidad y el modo
          var command = $rootScope.master.intencidad.toString();
              command += tablero.modo.toString();

          // Si es 1 o 2 no se va a mostrar los slots izquierdos
          if( tablero.modo > 0 ) {
              command += '   ';
          } else {
              command += ( ( tablero.si.length < 3 ) ? tablero.si + ' '.repeat( 3 - tablero.si.length ) : tablero.si );
          }

          command += tablero.ud.toString().replace('-', ' ').toUpperCase();

          command += tablero.puntos;

          var sd = ( ( tablero.sd.length < 5 ) ? tablero.sd + ' '.repeat( 5 - tablero.sd.length ) : tablero.sd );
          command += sd.toUpperCase()

          command = command.substr(0,12) + '\r';

          $rootScope.lastCommand = command;
          $rootScope.$broadcast('command', command);
        }

        $rootScope.$on('tablero change', function(){
          $rootScope.display();
        })

        // Control de streaming global
        $rootScope._timeouts = $rootScope._intervals = [];

        $rootScope.addInterval = function( interval ) {
            $rootScope._intervals.push(interval);
        }
        $rootScope.addTimeout = function( timeout ) {
            $rootScope._timeouts.push(timeout);
        }

        $rootScope.stopAll = function() {
            angular.forEach($rootScope._timeouts, function(to){
                    clearTimeout(to);
            })
            angular.forEach($rootScope._intervals, function(to){
                    clearInterval(to);
            })
        }

       // $socket.emit('start:crono')
    })

TabaApp.controller('Settings', function($scope,$rootScope, $socket){

  $scope.server = '192.168.0.101';
  $scope.ip = 2323;

  $scope.changeServer = function() {
    $socket.change('http://' + $scope.server +':' + $scope.ip);
  }

})

TabaApp.controller('AppManager', function($scope,$rootScope){
  $scope.tabs = [
    { title : 'Comando', icon:'terminal icon', tpl:'view/modo.comando.html', active: true},
    { title : 'Tiempo', icon:'wait icon', tpl:'view/modo.tiempo.html'},
    { title : 'Partidor', icon:'browser icon', tpl:'view/modo.partidor.html'},
    { title : 'Cuenta Regresiva', icon:'wait horizontally flipped icon', tpl:'view/modo.regresiva.html'},
    { title : 'Marquesina', icon:'keyboard icon', tpl:'view/modo.marquesina.html'},
    { title : "Config", icon: 'setting icon', tpl:'view/setting.html'}
  ];

  $scope.active = function() {
    return $scope.tabs.filter(function(pane){
      return pane.active;
    })[0];
  };

  $scope.activateTab = function(tab) {
    $rootScope.$broadcast('change tab', tab);
  }

  $scope.availableSlotIzquierdo = function(){
    if( $scope.tablero.modo === '0' ) return true;
    return false;
  }

  $scope.$watch('master.intencidad', function(a){
    $rootScope.$broadcast('tablero intencidad', a);
  },true)

  $scope.$watchCollection('tablero', function(newValue){
    $rootScope.$broadcast('tablero change', newValue);
  })

})

TabaApp.controller('Regresive', function($scope, $rootScope, $socket){
  $scope.tablero = {
    intencidad : 1,
    modo : 0,
    si: 'ABC',
    ud: 'U',
    puntos: 0,
    sd: ''
  }

  var pad = function(i){
      var s = "" + i;
      return s.length == 1 ? "0" + s : s;
  }

  $scope.parseAlone = function(tablero) {
      // Define la intencidad y el modo
      var command = $rootScope.master.intencidad.toString();
          command += tablero.modo.toString();

      // Si es 1 o 2 no se va a mostrar los slots izquierdos
      if( tablero.modo > 0 ) {
          command += '   ';
      } else {
          command += ( ( tablero.si.length < 3 ) ? tablero.si + ' '.repeat( 3 - tablero.si.length ) : tablero.si );
      }

      command += tablero.ud.toString().replace('-', ' ').toUpperCase();

      command += tablero.puntos;

      var sd = ( ( tablero.sd.length < 5 ) ? tablero.sd + ' '.repeat( 5 - tablero.sd.length ) : tablero.sd );
      command += sd.toUpperCase()

      command = command.substr(0,12) + '\r';
      return command;
  }

  $scope.cuentaRegresiva = function() {
      var time = $scope.timeInit * 60; // --> paso a segundos


      var pad = function(i){
          var s = "" + i;
          return s.length == 1 ? "0" + s : s;
      }

      $scope.crono = setInterval( function() {
        $scope.$apply(function () {

          var seconds = pad( time % 60 );
          var m       = pad(parseInt(time/60)%60);
          var h       = parseInt(time/3600)%9;
          var p = 2;

          $scope.tablero.sd = h + "" + m + seconds;


          command = $scope.parseAlone($scope.tablero);

          $rootScope.$broadcast('command', command);

          if( time == 0 ) {
              clearInterval($s.stream);
          }
          time--;
        })
      }, 1000);
      $rootScope.addInterval($scope.crono);
  }

  $scope.stop = function(){
    $rootScope.stopAll();
    clearInterval($scope.crono);
  }


})

TabaApp.controller('Tiempo', function( $scope, $rootScope, $socket ){
  var date = new Date;
  var seconds = date.getSeconds();
  var minutes = date.getMinutes();
  var hour = date.getHours();

  $scope.time = hour + ':' + minutes ;
  $scope.tablero = {
    intencidad : 1,
    modo : 0,
    si: 'ABC',
    ud: 'U',
    puntos: 0,
    sd: ''
  }

  var pad = function(i){
      var s = "" + i;
      return s.length == 1 ? "0" + s : s;
  }
  var seconds = 0;
  var pad = function(i){
      var s = "" + i;
      return s.length == 1 ? "0" + s : s;
  }

  $scope.parseAlone = function(tablero) {
      // Define la intencidad y el modo
      var command = $rootScope.master.intencidad.toString();
          command += tablero.modo.toString();

      // Si es 1 o 2 no se va a mostrar los slots izquierdos
      if( tablero.modo > 0 ) {
          command += '   ';
      } else {
          command += ( ( tablero.si.length < 3 ) ? tablero.si + ' '.repeat( 3 - tablero.si.length ) : tablero.si );
      }

      command += tablero.ud.toString().replace('-', ' ').toUpperCase();

      command += tablero.puntos;

      var sd = ( ( tablero.sd.length < 5 ) ? tablero.sd + ' '.repeat( 5 - tablero.sd.length ) : tablero.sd );
      command += sd.toUpperCase()

      command = command.substr(0,12) + '\r';
      return command;
  }


  $scope.display = function(){
    $scope.crono = setInterval(function(){
      $scope.$apply(function () {
        var date = new Date;
        var m = date.getMinutes();
        var h = date.getHours();

        var sd = " "+pad(h) + pad(m);
        $scope.tablero.puntos = 1;
        $scope.tablero.sd = sd;

           command = $scope.parseAlone( $scope.tablero );
      //  if( $scope.streamCrono ){
            $rootScope.crono = $scope.crono;
            console.log('Tiempo->', command)
            $rootScope.$broadcast('command', command);
        //}
      })
    },1000);

    $rootScope.addInterval($scope.crono);
  }

  $scope.stop = function(){
    $rootScope.stopAll();
    clearInterval($scope.crono);
  }


})

TabaApp.controller('Partidor', function ($socket, $scope, $rootScope) {
    $scope.streamCrono = false;
    $scope.timeInterval = 15; //segundos
    $scope.initTime = 0;
    $scope.motos = [];
    $scope.listReady = function() {
      return $scope.motos.length > 0
    }
    $scope.tablero = {
        intencidad : $rootScope.master.intencidad,
        modo : 0,
        si: 'ABC',
        ud: ' ',
        puntos: 2,
        sd: ''
    }

    $scope.parse = function() {
        // Define la intencidad y el modo
        var command = $rootScope.master.intencidad.toString();
            command += $scope.tablero.modo.toString();

        // Si es 1 o 2 no se va a mostrar los slots izquierdos
        if( $scope.tablero.modo > 0 ) {
            command += '   ';
        } else {
            command += ( ( $scope.tablero.si.length < 3 ) ? $scope.tablero.si + ' '.repeat( 3 - $scope.tablero.si.length ) : $scope.tablero.si );
        }

        command += $scope.tablero.ud.toString().replace('-', ' ').toUpperCase();

        command += $scope.tablero.puntos;

        var sd = ( ( $scope.tablero.sd.length < 5 ) ? $scope.tablero.sd + ' '.repeat( 5 - $scope.tablero.sd.length ) : $scope.tablero.sd );
        command += sd.toUpperCase()

        command = command.substr(0,12) + '\r';

        return command;
    }

    $scope.parseAlone = function(tablero) {
        // Define la intencidad y el modo
        var command = $rootScope.master.intencidad.toString();
            command += tablero.modo.toString();

        // Si es 1 o 2 no se va a mostrar los slots izquierdos
        if( tablero.modo > 0 ) {
            command += '   ';
        } else {
            command += ( ( tablero.si.length < 3 ) ? tablero.si + ' '.repeat( 3 - tablero.si.length ) : tablero.si );
        }

        command += tablero.ud.toString().replace('-', ' ').toUpperCase();

        command += tablero.puntos;

        var sd = ( ( tablero.sd.length < 5 ) ? tablero.sd + ' '.repeat( 5 - tablero.sd.length ) : tablero.sd );
        command += sd.toUpperCase()

        command = command.substr(0,12) + '\r';
        return command;
    }

    $scope.showContent = function($fileContent){
        var lines      = $fileContent.split("\n");
        $scope.motos   = lines.filter(Boolean);
        $scope.assignTime();

        $scope.content = $fileContent;
    };

    $scope.assignTime = function(){



        angular.forEach($scope.motos, function(moto, idx){

            if( typeof moto == "string") {
               $scope.motos[idx] = {
                    nro : moto,
                    time : idx * $scope.timeInterval
                }
            } else {
                $scope.motos[idx].time = idx * $scope.timeInterval
            }
        })
    }

    $scope.largar = function() {
        $scope.iniciado = true;
        $scope.streamCrono = false;
        $scope.timeout = [];

        $scope.motos.forEach(function( moto ){
            var to = setTimeout(function(){
              $scope.$apply(function () {
                    $scope.cuentaRegresiva(moto, $scope);
              })
            }, ( ( moto.time + 2 ) * 1000) );

            $scope.timeout.push(to);
            $rootScope.addTimeout(to);
            if( moto.time == 0 ) {
                // Primera moto
                // cuando termine la cuentaregresiva empieza el cronometro
                // de la carrera
                var initTimer = $scope.timeInterval ;
                var timeoutCrono = setTimeout(function(){
                  $scope.$apply(function () {
                    $scope.initCrono()
                  })
                }, initTimer * 1000 );

                $scope.timeout.push(timeoutCrono);
                $rootScope.addTimeout(timeoutCrono);
            }
        })
    }

    $scope.initCrono = function(){
        var seconds = 0;
        var pad = function(i){
            var s = "" + i;
            return s.length == 1 ? "0" + s : s;
        }

        $scope.crono = setInterval(function(){
          $scope.$apply(function () {
            var s = pad(seconds%60);
            var m = pad(parseInt(seconds/60)%60);
            var h = parseInt(seconds/(60*60));

            seconds++;
            var sd = ""+h + m + s;

            var tablero = {
                intencidad : 1,
                modo : 1,
                si: '   ',
                ud: ' ',
                puntos: 2,
                sd: sd
            }

            command = $scope.parseAlone( tablero );
            if( $scope.streamCrono ){
                $rootScope.crono = $scope.crono;
                $rootScope.$broadcast('command', command);
            }
          })
        },1000);

        $rootScope.addInterval($scope.crono);
    }


    $scope.stop = function() {
        $scope.iniciado = false;
        angular.forEach($scope.timeout, function(to){
            clearTimeout(to);
        })
        clearInterval($scope.stream);
        clearInterval($scope.crono);
    }

    $scope.cuentaRegresiva = function( moto, $s ) {
        var time = $scope.timeInterval - 2;

        var pad = function(i){
            var s = "" + i;
            return s.length == 1 ? "0" + s : s;
        }

        $s.stream = setInterval( function() {
          $scope.$apply(function () {
            var p = 2;
            if( time > 5 ) {
                var sd = "0" + "00" + pad(time);
            } else {
                p  = 0;
                if( time > 0 ) {
                    var sd = " ".repeat(time - 1 ) + '*'.repeat( 6 - time);
                }else {
                    var sd = " ".repeat(5)
                }
            }

            var nro = moto.nro.toString().replace(/(\r\n|\n|\r)/gm,"");

            $scope.tablero = {
                intencidad : 1,
                modo : 0,
                si: nro,
                ud: ' ',
                puntos: p,
                sd: sd
            }
            command = $scope.parse();

            $rootScope.stream_cuenta = $scope.stream;
            $rootScope.$broadcast('command', command);

            if( time == 0 ) {
                clearInterval($s.stream);
                var last = $scope.motos[$scope.motos.length-1];
                if( moto == last ) {
                    $scope.streamCrono = true;
                }
            }
            time--;
          })
        }, 1000);
    }

  });

TabaApp.directive('onReadFile', function ($parse) {
    return {
        restrict: 'A',
        scope: false,
        link: function(scope, element, attrs) {
            var fn = $parse(attrs.onReadFile);

            element.on('change', function(onChangeEvent) {
                var reader = new FileReader();

                reader.onload = function(onLoadEvent) {
                    scope.$apply(function() {
                        fn(scope, {$fileContent:onLoadEvent.target.result});
                    });
                };

                reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
            });
        }
    };
});


TabaApp
    .controller('Ctrl', function Ctrl($scope, $socket){

        $scope.send = function() {
            var command = $scope.tablero.command.substr(0,12) + '\r';
            $rootScope.$broadcast('command', command);
        }
    })
    .controller('Comando', function($scope, $socket){
        var connected = true;
        $socket.on('tablero.disconnect', function(){
            connected = false;
        })

        $scope.tablero = {
            intencidad : 1,
            modo : 0,
            si: 'ABC',
            ud: 'U',
            puntos: 0,
            sd: ''
        }

        $scope.send = function(){
          //$socket.emit('command', command );
        }

        $scope.parse = function() {
            // Define la intencidad y el modo
            var command = $scope.tablero.intencidad.toString();
                command += $scope.tablero.modo.toString();

            // Si es 1 o 2 no se va a mostrar los slots izquierdos
            if( $scope.tablero.modo > 0 ) {
                command += '   ';
            } else {
                command += ( ( $scope.tablero.si.length < 3 ) ? $scope.tablero.si + ' '.repeat( 3 - $scope.tablero.si.length ) : $scope.tablero.si );
            }

            command += $scope.tablero.ud.toString().replace('-', ' ').toUpperCase();

            command += $scope.tablero.puntos;

            var sd = ( ( $scope.tablero.sd.length < 5 ) ? $scope.tablero.sd + ' '.repeat( 5 - $scope.tablero.sd.length ) : $scope.tablero.sd );
            command += sd.toUpperCase()

            command = command.substr(0,12) + '\r';
            if( connected ) {
                $rootScope.$broadcast('command', command);
            } else {
                $socket.emit('try connect');
            }
        }

        $scope.tryconnect = function() {
            $socket.emit('try reconnect')
        }
    })

    .controller('Marquesina', function($scope, $socket, $rootScope){
        $scope.tablero = {
            modo : 0,
            si: '   ',
            ud: '-',
            puntos: 0,
            sd: ''
        }

        $scope.duration = 300;

        $scope.stop = function(){
          $rootScope.stopAll();
          clearInterval($scope.interval);
        }

        $scope.parse = function() {
            clearInterval($scope.interval);


            var strlen = $scope.tablero.sd.length;
            var current = 0;
            var marquee = ' '.repeat(5) + $scope.tablero.sd.toUpperCase() + ' '.repeat(10) + ' '.repeat(5)

            $scope.interval = setInterval(function(){
              $scope.$apply(function () {
                // Define la intencidad y el modo
                var command = $rootScope.master.intencidad.toString();
                    command += $scope.tablero.modo.toString();

                // Si es 1 o 2 no se va a mostrar los slots izquierdos
                if( $scope.tablero.modo > 0 ) {
                    command += '   ';
                } else {
                    command += ( ( $scope.tablero.si.length < 3 ) ? $scope.tablero.si + ' '.repeat( 3 - $scope.tablero.si.length ) : $scope.tablero.si );
                }

                command += $scope.tablero.ud.toString().replace('-', ' ').toUpperCase();

                command += $scope.tablero.puntos;

                var mcommand = command + marquee.substr(current,5);
                mcommand = mcommand.substr(0,12) + '\r';
                console.log('emit command', mcommand)
                $rootScope.$broadcast('command', mcommand);
                if( current + 1 == marquee.length - 5 ) {
                    current = 0;
                } else {
                    current++;
                }
              })
            },$scope.duration)

            $rootScope.addInterval($scope.interval);
        }
    })

    .controller('Stream', function($scope, $socket){

    })

    .filter('HMMSS', ['$filter', function ($filter) {
        return function (input, decimals) {
            var sec_num = parseInt(input, 10),
                decimal = parseFloat(input) - sec_num,
                hours   = Math.floor(sec_num / 3600),
                minutes = Math.floor((sec_num - (hours * 3600)) / 60),
                seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours   < 10) {hours   = "0"+hours;}
            if (minutes < 10) {minutes = "0"+minutes;}
            if (seconds < 10) {seconds = "0"+seconds;}
            var time    = hours+':'+minutes+':'+seconds;
            if (decimals > 0) {
                time += '.' + $filter('number')(decimal, decimals).substr(2);
            }
            return time;
        };
    }])

TabaApp.controller('Display', function($socket, $scope){
  $scope.$on('show',function(data){
    console.log('display: ' + data);
  })
})

TabaApp.controller('Hora', function($socket, $scope){
  $scope.emmit
})

TabaApp.factory('EventsService',[function( $rootScope ){


  return {
    onActive : function( $scope, handler ) {
      $scope.$on('onActive', function( event, data ){
        handler(data);
      })
    }
  }
}])


TabaApp.directive('ledDisplay', ['$document', '$filter', '$log', function( $document, $filter, $log){

  var eventNs = '.tabaCrono',
      scopeOptions = {
        model: '=?'
      }

  return {
      restrict :'A',
      replace : true,
      template:'<label>{{model}}</label>',
      scope: {
        model : '=?'
      },
      link : function(scope, element, attrs, controller) {
        scope.$watch('model', function(nv, ov){
          scope.model = nv ;
        })
      }
  }


}])

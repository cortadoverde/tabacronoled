var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('sass', function(){
  gulp.src('./assets/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css/'))
})

"use strict";

/**
 * Dependencias
 */
var express   = require('express'),
        guid  = require('./lib/uuid.js'),
          os  = require('os'),
        app   = express(),
     server   = require('http').createServer(app),
         io   = require('socket.io')(server),
       port   = process.env.PORT || 2323,
 AppClient    = require('./lib/io.listener'),
 Timer        = require('./lib/timer'),
 isMTReady    = false,
 isTCPReady   = false,
 modosActivos = [],
 streams      = {},
 inStream     = null; // uuid del steam activo

 var crono = new Timer();



/**
 * ManagerTablero
 * Procesa las salidas al protocolo TCP y la conexion con el tablero fisico,
 * a diferencia del ManagerEvent que gestiona las solicitudes de los clientes
 */
var ManagerTablero  = require('socket.io-client')('http://localhost:2324');


server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

app.use(express.static(__dirname + '/public'));




io.sockets.on('connection', function(socket) {
    AppClient.set(socket);
    socket.on('disconnect', function(){
        AppClient.disconnect();
    })
})

AppClient.on('ready', function(self){

    this.socket.on('command', function(data){
        console.log('send to MT ' + data);
        ManagerTablero.emit('command', data);
    })

    this.socket.on('try reconnect', function(){
        console.log('tratando de establecer conexion con el tablero');
        ManagerTablero.emit('connect tablero');
    })

    this.socket.on('mode crono', function(){

    })

    this.socket.on('start:crono', function(){
        crono.start();
        crono.on('tick', function(time){
            console.log(time);
        })
    })

    // crea un intervalo de tiempo con una funcion que devuelve
    // los comandos a enviar
    // debe contener los siguientes parametros
    // { process : function, close: function(),  }
    this.socket.on('stream', function(params){

    })
})


ManagerTablero.on('connect', function(socket){
    //COnexion con el MT
    console.log('conexion establecida con el MT');
    isMTReady = true;
});


ManagerTablero.on('tablero error', function(){
    if( AppClient.connected ){
        console.log('[notificar -> Control ' + AppClient.id +'] no hay conexion con el tablero');
        AppClient.socket.emit('noty', { type: 'error', msj: 'no hay conexion con el tablero' } );
    }
})

ManagerTablero.on('tablero error command', function(){
    if( AppClient.connected ){
        console.log('[notificar -> Control ' + AppClient.id +'] No se puede enviar el comando, compruebe la conexion');
        AppClient.socket.emit('noty', { type: 'error', msj: 'No se puede enviar el comando, compruebe la conexion' } );
    }
})

ManagerTablero.on('tcp ready', function(){
    console.log('[INFO] TCP CONECTADO');
    isTCPReady = true;
    if( AppClient.connected ){
        AppClient.socket.emit('tcp ready')
    }
})

ManagerTablero.on('tcp off', function( from ){
    console.log('[INFO] TCP NO CONECTADO -> ' + from);
    isTCPReady = false;
    if( AppClient.connected ){
        AppClient.socket.emit('tcp off')
    }
})
